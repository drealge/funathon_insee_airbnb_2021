# funathon_insee_airbnb

Espace projet de l'équipe DREAL Grand Est pour le Funathon organisé par l'Insee sur les données Airbnb, les 21 et 22 juin 2021.

<b> Les productions de l'équipe lors du funathon : </b>

* Document de présentation des productions : [presentation_groupe_5.Rmd](https://gitlab.com/drealge/funathon_insee_airbnb_2021/-/blob/main/presentation_groupe_5.Rmd)

* Création et alimentation d'une base postgis en Python : [charge_ficairbnb_postgre.ipynb](https://gitlab.com/drealge/funathon_insee_airbnb_2021/-/blob/main/charge_ficairbnb_postgre.ipynb)
* Récupérer des données d'une base postgis en R :
[Requetes_postgis.R](https://gitlab.com/drealge/funathon_insee_airbnb_2021/-/blob/main/Requetes_postgis.R)
* Import de données OpenStreetMap : [import_osm_guv.Rmd](https://gitlab.com/drealge/funathon_insee_airbnb_2021/-/blob/main/import_osm_guv.Rmd)
* Traitement du langage via DTM et construction d'une fonction de recherche de logements par mot dans le descriptif des annonces : [Traitement_langage.R](https://gitlab.com/drealge/funathon_insee_airbnb_2021/-/blob/main/Traitement_langage.R)
* Cartographie au carreau des logements de la fonction de recherche [Carto_langage.R](https://gitlab.com/drealge/funathon_insee_airbnb_2021/-/blob/main/Carto_langage.R)
* Application Rshiny permettant d'effectuer la recherche par mot et de visualiser la carte carroyée des logements correspondant : 
[app.R](https://gitlab.com/drealge/funathon_insee_airbnb_2021/-/blob/main/carte_mot_cle/app.R)
