### exporter des fichiers vers un bucket


library(aws.s3)

# envoi dans le répertoire diffusion du bucket thierryz :
put_object(file = "proverbe.jpg", bucket = "thierryz/diffusion",
           region = "")

# liste des fichiers initiaux, d'après leur extension
l_fich <- c(list.files(pattern = "*.csv*"),
            list.files(pattern = "*.gpkg"))

# dépot dans le dossier **diffusion/fichiersinitiaux**
for(i_nf in l_fich) {
  put_object(file = i_nf,
             bucket = "thierryz/diffusion/fichiersinitiaux",
             region = "")
}

# et voila...